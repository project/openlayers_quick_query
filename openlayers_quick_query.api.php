<?php

function hook_openlayers_quick_query_render_alter(&$render, $view) {
  foreach ($render as $key => $val) {
    $row = &$render[$key];

    $field = 'description';
    $attr = $row['attributes'][$field];

    if (!is_string($attr)) {
      $row['attributes'][$field] = mymodule_render_plain_description($attr);
    }

    $field = 'rich_description';
    $attr = $row['attributes'][$field];

    if (is_array($attr)) {
      $row['attributes'][$field] = mymodule_render_rich_description($attr['content'], $attr['row']);
    }
  }
}

function hook_openlayers_quick_query_info($view) {
  if ($view->name == 'my_map') {

    // Add projection and wkt information
    $info['projection'] = '4326';
    $info['wkt_field'] = 'field_geolocation';

    // Add needed fields
    $info['fields'][] = array(
      'field_name' => 'field_geolocation'
    );

    $info['fields'][] = array(
      'field_name' => 'field_description',
      'additional_fields' => FALSE  // Return only default property of the field. It will automatically be processed with check_plain.
    );

    $info['fields'][] = array(
      'field_name' => 'field_rich_description',
      'additional_fields' => TRUE  // Return all properties of the field. Note: This may slow down the query - depending on the field
    );

    $info['attributes'] = array(
      'nid' => 'nid',
      'name' => 'node_link_fast', // Special very fast node_link linking the title using just node/<nid> with no aliases
      'description' => 'field_description', // Give back the description field as field_description
      'rich_description' => 'field_rich_description' // Save the rich description field as "rich_description" an additional property that can be used by custom OL plugins
    );

    return $info;
  }
}
